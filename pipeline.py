#!/usr/bin/env python
from fsl.utils.filetree import FileTree, MissingVariable
from mcutils.pipe import generates
from pathlib import Path
import glob
from subprocess import call, run
import nibabel as nib
import numpy as np
from mcutils.utils.log import setup_log
from mcutils.utils.sidecar import AcquisitionParams, concat
from mcutils.scripts.split.submit import run as submit_split
import logging
import os.path as op
from mcutils.cudimot import cudimot
logger = logging.getLogger(__name__)


@generates(['input_topup_full', 'topup_full/fieldcoef', 'topup_full/movement'],
           logdir='log')
def topup_full(tree):
    """
    Runs topup on the AP/PA b0s (in that order)

    The other data was acquired using AP
    """
    input_fn = tree.get('input_topup_full', make_dir=True)
    call(['fslmerge', '-t', input_fn, tree.get('b0_AP'), tree.get('b0_PA')])

    call(['topup', f'--imain={input_fn}', f'--datain={tree.get("acqparams")}',
          f'--out={tree.get("topup_full/basename")}', f'--iout={tree.get("topup_corr_full")}',
          '--config=b02b0.cnf', '-v'])


@generates(['nodif_full/input', 'nodif_full/output', 'nodif_full/mask'],
           logdir='log')
def topup_full_mask(tree):
    """
    Creates mask from the reformed b0 image
    """
    call(['applytopup', f'--imain={tree.get("b0_AP")},{tree.get("b0_PA")}',
          f'--topup={tree.get("topup_full/basename")}', '--inindex=1,2',
          f'--out={tree.get("nodif_full/input")}', '-a', tree.get("acqparams")])
    call(['bet', tree.get('nodif_full/input'), tree.get('nodif_full/output'), '-m',
          '-g', '0.3', '-f', '0.3'])


@generates(['nodif2T1w/transform', 'T1w_2mm', 'T1w_2mm_partial', 'T1w_2mm_partial_mask'],
           queue='short.q', logdir='log')
def prep_struct(tree):
    """
    Process the structural T1-weighted image
    """
    run([
        'bet', tree.get('T1w'), tree.get('T1w_bet/output', make_dir=True), '-m'
    ])
    run([
        'epi_reg',
        f"--epi={tree.get('nodif_full/input')}",
        f"--t1={tree.get('T1w')}",
        f"--t1brain={tree.get('T1w_bet/output')}",
        f"--out={tree.get('nodif2T1w/basename', make_dir=True)}",
    ])
    run([
        'flirt', '-in', tree.get('T1w_bet/mask'), '-ref', tree.get('T1w'), '-applyisoxfm', '2',
        '-out', tree.get('T1w_2mm_partial_mask'), '-interp', 'nearestneighbour'
    ])
    run([
        'fslroi', tree.get('T1w_2mm_partial_mask'), tree.get('T1w_2mm_partial_mask'), '0', '-1', '0', '-1', '39', '29'
    ])
    run([
        'flirt', '-in', tree.get('T1w'), '-ref', tree.get('T1w'), '-applyisoxfm', '2',
        '-out', tree.get('T1w_2mm')
    ])
    run([
        'fslroi', tree.get('T1w_2mm'), tree.get('T1w_2mm_partial'), '0', '-1', '0', '-1', '39', '29'
    ])
    run([
        'fslmaths', tree.get('T1w_2mm_partial'), '-roi', '0', '-1', '0', '-1', '3', '20', '0', '-1', tree.get('T1w_2mm_partial')
    ])


@generates(['ref_b0', 'b0_transform', 'partial_b0_AP', 'partial_b0_PA', 'eddy_mask'],
           logdir='mod_log', queue='short.q')
def register_b0(tree):
    """
    Registers the full-brain b0 to the partial-brain images
    """
    img = nib.load(tree.get('raw_image'))
    bvals = np.genfromtxt(tree.get('bvals_link'))
    data = img.dataobj[..., int(np.where(bvals < 200)[0][0])]
    nib.Nifti1Image(data, img.affine).to_filename(tree.get('ref_b0', make_dir=True))
    call(['flirt', '-in', tree.get('b0_AP'), '-ref', tree.get('ref_b0'),
          '-omat', tree.get('b0_transform'), '-out', tree.get('partial_b0_AP')])
    call(['flirt', '-in', tree.get('b0_PA'), '-ref', tree.get('ref_b0'),
          '-applyxfm', '-init', tree.get('b0_transform'), '-out', tree.get('partial_b0_PA'),
          '-dof', '6'])
    call([
        'flirt', '-in', tree.get('nodif_full/mask'), '-ref', tree.get('ref_b0'),
        '-applyxfm', '-init', tree.get('b0_transform'), '-out', tree.get('eddy_mask', make_dir=True),
        '-interp', 'nearestneighbour',
    ])


@generates(['input_topup', 'topup/fieldcoef', 'topup/movement'],
           logdir='mod_log')
def topup(tree):
    """
    Runs topup on the AP/PA b0s (in that order)

    The other data was acquired using AP
    """
    input_fn = tree.get('input_topup', make_dir=True)
    call(['fslmerge', '-t', input_fn, tree.get('partial_b0_AP'), tree.get('partial_b0_PA')])

    call(['topup', f'--imain={input_fn}', f'--datain={tree.get("acqparams")}',
          f'--out={tree.get("topup/basename")}', f'--iout={tree.get("topup_corr")}',
          '--config=b02b0.cnf', '-v'])


@generates(['index', 'eddy/image'], logdir='mod_log', queue='cuda.q')
def eddy(tree):
    """
    Runs eddy for each sub-set of the diffusion data
    """
    bvals = np.genfromtxt(tree.get('bvals_link'), dtype='i4')
    bvals[bvals < 200] = 0
    np.savetxt(tree.get('eddy_bvals', make_dir=True), bvals, fmt='%i')
    img = nib.load(tree.get('raw_image'))
    nvolumes = img.shape[3]
    np.savetxt(tree.get('index', make_dir=True), np.ones(nvolumes, dtype='i4'), fmt='%d')
    call(['eddy_cuda', f"--imain={tree.get('raw_image')}", f"--mask={tree.get('eddy_mask')}",
          f"--index={tree.get('index')}", f"--acqp={tree.get('acqparams')}",
          f"--bvecs={tree.get('bvecs_link')}", f"--bvals={tree.get('eddy_bvals')}",
          f"--topup={tree.get('topup/basename')}", f"--out={tree.get('eddy/basename', make_dir=True)}",
          '--data_is_shelled'])


@generates(['clean', 'eddy2T1w'], logdir='mod_log', queue='veryshort.q')
def register_eddy(tree):
    """
    Moves the files from eddy to the Diffusion directory
    """
    call([
        'convert_xfm', '-omat', tree.get('inverse_b0_transform', make_dir=True),
        '-inverse', tree.get('b0_transform')
    ])
    call([
        'convert_xfm', '-omat', tree.get('eddy2T1w', make_dir=True),
        '-concat', tree.get('nodif2T1w/transform'), tree.get('inverse_b0_transform')
    ])
    call([
        'flirt', '-in', tree.get('eddy/image'), '-ref', tree.get('T1w_2mm'),
        '-applyxfm', '-init', tree.get('eddy2T1w'), '-out', tree.get('clean', make_dir=True)
    ])
    run([
        'fslroi', tree.get('clean'), tree.get('clean'), '0', '-1', '0', '-1', '38', '29'
    ])


@generates(['image', 'sidecar'], queue='veryshort.q', logdir='log')
def merge(tree: FileTree):
    """
    Merges all the data into a single file
    """
    acquisition = [
        ('std', (0, 100, 0)),
        ('short_TE', (1, 100, 0)),
        ('long_TE', (1, 150, 0)),
        ('long_diff', (1, 150, 1)),
    ]
    params = []
    images = []
    for type, (tr, te, tm) in acquisition:
        tree_type = tree.update(type=type)
        filenames = tree_type.get_all('clean', glob_vars=('modality', ))
        images.extend(filenames)
        for fn in filenames:
            tree_mod = tree_type.update(**tree_type.extract_variables('clean', fn))
            xps = AcquisitionParams.read_bvals_bvecs(
                    tree_mod.get('bvals_link'),
                    tree_mod.get('eddy/rotated_bvecs'),
                    tree_mod.variables['modality']
            )
            xps.update(tr=tr, te=te, tm=tm)
            params.append(xps)
            logger.debug(f'Appending {fn} as modality {type} (tr={tr}, te={te}, tm={tm})')
    assert len(params) == len(images)
    full_params = concat(*params)
    full_params.write(tree.get('sidecar', make_dir=True))
    run(['fslmerge', '-t', tree.get('image', make_dir=True)] + images)


@generates(['fov_mask'], queue='veryshort.q', logdir='log')
def fov_mask(tree: FileTree):
    """
    Creates a mask covering only those voxels within all FOVs
    """
    for tree_sub in tree.get_all_trees('eddy2T1w', glob_vars=('modality', 'type')):
        fov_mask = tree_sub.get('fov_mask_mod')
        img = nib.load(tree_sub.get('eddy/image'))
        within = np.ones(img.shape[:3], dtype=int)
        for idx in (0, -1):
            for dim in range(3):
                slcs = [slice(None)] * 3
                slcs[dim] = idx
                within[tuple(slcs)] = 0
        nib.Nifti1Image(within, None, header=img.header).to_filename(fov_mask)
        call([
            'flirt', '-in', fov_mask, '-ref', tree.get('T1w_2mm'),
            '-applyxfm', '-init', tree_sub.get('eddy2T1w'), '-out', fov_mask
        ])
        run([
            'fslroi', fov_mask, fov_mask, '0', '-1', '0', '-1', '38', '29'
        ])
        run([
            'fslmaths', fov_mask, '-thr', '0.99', '-bin', fov_mask
        ])

    fov_mask = tree.get('fov_mask')
    run(
        ['fsladd', fov_mask, '-m'] + list(tree.get_all('fov_mask_mod', glob_vars=('modality', 'type')))
    )
    run(['fslmaths', fov_mask, '-thr', '0.999', '-bin', fov_mask])


@generates(['mean_signal', 'mean_signal_json', 'anisotropy', 'anisotropy_json'],
           logdir='log', queue='short.q')
def micro_anisotropy(tree: FileTree):
    """
    Computes the micro-anisotropy
    """
    run([
        'mc_script', 'MDE', 'spherical_mean', tree.get('image'), tree.get('mean_signal', make_dir=True), 'sample',
        '-x', tree.get('sidecar'),
        '--b_symm=200',
        '--b_perp=200',
        '--te=0',
        '--tr=0',
        '--tm=0',
    ])
    run([
        'mc_script', 'MDE', 'micro_anisotropy',
        tree.get('mean_signal'), tree.get('mean_signal_json'),
        tree.get('anisotropy', make_dir=True), tree.get('anisotropy_json', make_dir=True),
        '-m', tree.get('T1w_2mm_partial_mask'),
        '--Sbase', tree.get('Sbase', make_dir=True),
        '--te=0',
        '--tr=0',
        '--tm=0',
        '--b=200',
    ])


@generates(['disp/microA', 'disp/dyad', 'disp/dyad_disp'], logdir='log', queue='verylong.q')
def fit_dispersion(tree: FileTree):
    """
    Fits dispersing zeppelin model
    """
    cmd = [
        'mc_script', 'MDE', 'fit_dispersion', tree.get('image'), tree.get('sidecar'),
        tree.get('disp/basename', make_dir=True),
        '--DTI', tree.get('dti/basename'),
        '-m', tree.get('T1w_2mm_partial_mask'),
        '--te=0',
        '--tr=0',
        '--tm=0',
        '--b=200',
    ]
    logger.info(' '.join(cmd))
    run(cmd)


@generates(['data_b1500', 'dti/V1', 'dti/V2', 'dti/V3'], logdir='log', queue='short.q')
def dti(tree: FileTree):
    """
    Fits DTI to the LTE data
    """
    mod_tree = tree.update(modality='LTE', type='std')
    bvals = np.genfromtxt(mod_tree.get('bvals_link'))
    use = (abs(bvals - 1500) < 100) | (bvals < 300)
    np.savetxt(tree.get('bvals_b1500', make_dir=True), bvals[use])

    bvecs = np.genfromtxt(mod_tree.get('eddy/rotated_bvecs'))
    np.savetxt(tree.get('bvecs_b1500'), bvecs[:, use])

    img = nib.load(mod_tree.get('clean'))
    data = img.get_data()[..., use]
    nib.Nifti1Image(data, img.affine).to_filename(mod_tree.get('data_b1500'))

    run([
        'dtifit',
        '-k', mod_tree.get('data_b1500'),
        '-r', mod_tree.get('bvecs_b1500'),
        '-b', mod_tree.get('bvals_b1500'),
        '-m', mod_tree.get('T1w_2mm_partial_mask'),
        '-o', mod_tree.get('dti/basename', make_dir=True),
    ])


@generates(['S0'], logdir='log', queue='veryshort.q')
def get_S0(tree: FileTree):
    """
    Extracts the mean S0
    """
    run([
        'select_dwi_vols', tree.get('clean'), tree.get('bvals_link'), tree.get('S0', make_dir=True), '100', '-m',
    ])


@generates(['fast_orig/wm_pve', 'fast_2mm/wm_pve', 'tissue_mask', 'WM_mask', 'GM_mask',
            'CSF_pve', 'CSF_nopve', 'tissue_nopve'], logdir='log', queue='short.q')
def fast(tree: FileTree):
    """
    Runs FAST on the structural and transforms the result to the diffusion space
    """
    input = tree.get('fast_orig/input', make_dir=True)
    if not op.isfile(input):
        Path(input).symlink_to(op.relpath(tree.get('T1w_bet/output'), start=Path(input).parent))
    run([
        'fast', '-v', tree.get('fast_orig/input')
    ], check=True)
    for label in tree.sub_trees['fast_orig'].templates.keys():
        try:
            if op.isfile(tree.get(f'fast_orig/{label}')):
                run([
                    'flirt', '-in', tree.get(f'fast_orig/{label}'), '-ref', tree.get(f'T1w'), '-applyisoxfm', '2',
                    '-out', tree.get(f'fast_2mm/{label}', make_dir=True), '-interp', 'nearestneighbour'
                ], check=True)
                run([
                    'fslroi', tree.get(f'fast_2mm/{label}'), tree.get(f'fast_2mm/{label}'), '0', '-1', '0', '-1', '39', '29'
                ], check=True)
                run([
                    'fslmaths', tree.get(f'fast_2mm/{label}'), '-roi', '0', '-1', '0', '-1', '3', '20', '0', '-1',
                    tree.get(f'fast_2mm/{label}')
                ], check=True)
            else:
                logger.info(f'skipping {label} in mapping to 2mm')
        except MissingVariable:
            logger.info(f'skipping {label} in mapping to 2mm')
            continue
    run([
        'fslmaths', tree.get('fast_2mm/wm_pve'), '-thr', '0.5', '-bin',
        '-mul', tree.get('fov_mask'), tree.get('WM_mask', make_dir=True)
    ])
    run([
        'fslmaths', tree.get('fast_2mm/gm_pve'), '-thr', '0.5', '-bin',
        '-mul', tree.get('fov_mask'), tree.get('GM_mask', make_dir=True)
    ])
    run([
        'fslmaths', tree.get('fast_2mm/wm_pve'), '-add', tree.get('fast_2mm/gm_pve'),
        '-thr', '0.5', '-bin',
        '-mul', tree.get('fov_mask'), tree.get('tissue_mask', make_dir=True)
    ])
    run([
        'fslmaths', tree.get('fast_2mm/wm_pve'), '-add', tree.get('fast_2mm/gm_pve'),
        '-thr', '0.1', '-uthr', '0.9', '-bin',
        '-mul', tree.get('fov_mask'), tree.get('CSF_pve', make_dir=True)
    ])
    run([
        'fslmaths', tree.get('fast_2mm/wm_pve'), '-add', tree.get('fast_2mm/gm_pve'),
        '-thr', '0.9', '-bin',
        '-mul', tree.get('fov_mask'), tree.get('tissue_nopve', make_dir=True)
    ])
    run([
        'fslmaths', tree.get('fast_2mm/csf_pve'), '-thr', '0.9', '-bin',
        '-mul', tree.get('fov_mask'), tree.get('CSF_nopve', make_dir=True)
    ])


@generates(['disp_tissue_mask/dispA'], logdir='log', queue='short.q')
def mask_disp(tree):
    """
    For the dispersing zeppeling model mask out the CSF
    """
    for short_name in tree.sub_trees['disp'].templates.keys():
        print(short_name)
        if tree.on_disk(['disp/' + short_name]):
            run([
                'fslmaths', tree.get('disp/' + short_name),
                '-mul', tree.get('tissue_mask'),
                tree.get('disp_tissue_mask/' + short_name, make_dir=True)
            ])


@generates(['modality_mean_signal', 'lte_ste_ratio'], queue='short.q', logdir='log')
def lte_ste_ratio(tree: FileTree):
    """
    Computes the ratio of LTE over ste
    """
    for modality in ('LTE', 'STE'):
        tree_mod = tree.update(modality=modality)
        run([
            'mc_script', 'MDE', 'spherical_mean',
            tree_mod.get('clean'),
            tree_mod.get('modality_mean_signal', make_dir=True),
            'sample',
            '-b', tree_mod.get('bvals_link'),
            '--b=200', 
            '--sort=b',
        ], check=True)
    run([
        'fslmaths',
        tree.update(modality='LTE').get('modality_mean_signal'),
        '-div',
        tree.update(modality='STE').get('modality_mean_signal'),
        tree.get('lte_ste_ratio', make_dir=True)
    ], check=True)


def main():
    """
    Processes the MDE data
    """
    modalities = ('LTE', 'STE')
    types = ('std', 'short_TE', 'long_TE', 'long_diff')

    setup_log()
    tree_base = FileTree.read('day10')

    eddy_jobs = []
    for subject in ('A', 'B'):
        tree = tree_base.update(subject=subject)
        topup_full_job = topup_full(tree)
        topup_full_mask_job = topup_full_mask(tree, wait_for=topup_full_job)
        topup_struct_job = prep_struct(tree, wait_for=topup_full_mask_job)
        for type in types:
            logger.info('Evaluating {}'.format(type))
            modality_skipped = False
            for modality in modalities:
                tree_mod = tree.update(modality=modality, type=type)
                if not tree_mod.on_disk('raw_image'):
                    modality_skipped = True
                    logger.info(f'Skipping {modality} ({type}) for {subject}')
                    continue
                logger.info(f'Preprocessing {modality} ({type}) for {subject}')
                register_b0_job = register_b0(tree_mod, wait_for=topup_full_mask_job)
                topup_job = topup(tree_mod, wait_for=register_b0_job)
                eddy_job = eddy(tree_mod, wait_for=topup_job)
                eddy_jobs.extend(register_eddy(tree_mod, wait_for=topup_struct_job + eddy_job))

                if modality == 'LTE':
                    logger.info('running NODDI')
                    S0_job = get_S0(tree_mod, wait_for=tuple(eddy_jobs[-1:]))
                    cudimot(tree_mod.sub_trees['noddi'], '../clean', '../T1w_2mm_partial_mask',
                            cfp=('../eddy/rotated_bvecs', '../bvals_link'), fixp=('../S0', ),
                            model_name='NODDI_Bingham', wait_for=S0_job, optional_flags='--runMCMC --rician',
                            cudimot_dir='/home/fs0/ndcn0236/scratch/source/CUDIMOT', overwrite=False)
                    cudimot(tree_mod.sub_trees['ball_racket'], '../clean', '../T1w_2mm_partial_mask',
                            cfp=('../eddy/rotated_bvecs', '../bvals_link'), fixp=('../S0', ),
                            model_name='Ball_1_Racket', wait_for=S0_job, optional_flags='--runMCMC --rician',
                            cudimot_dir='/home/fs0/ndcn0236/scratch/source/CUDIMOT', overwrite=False)
            if not modality_skipped:
                lte_ste_ratio(tree.update(type=type), wait_for=eddy_jobs)

        dti_job = dti(tree, wait_for=tuple(eddy_jobs))
        merge_job = merge(tree, wait_for=tuple(eddy_jobs))

        fov_job = fov_mask(tree, wait_for=tuple(eddy_jobs))
        fast(tree, wait_for=fov_job)

        logger.info('Analyzing {}'.format(subject))
        micro_anisotropy(tree, wait_for=merge_job)
        for disp_flag, flag in [
#            ('gauss', ''),  #  uncomment this line to fit the data using Guassian noise model in addition to Rician noise model
            ('rician', '--ncoils=1'),
        ]:
            tree_disp = tree.update(disp_flag=disp_flag)
            if not all(tree_disp.get_all(name, glob_vars='all') for name in ('disp/microA', 'disp/dyad')):
                disp_job = submit_split(20, tree_disp.get('T1w_2mm_partial_mask'),
                             cmd=f"mc_script MDE fit_dispersion {tree_disp.get('image')} {tree_disp.get('sidecar')} " +
                                 f"{tree_disp.get('disp/basename', make_dir=True)}JOBID --DTI {tree_disp.get('dti/basename')} " +
                                 f"-m MASK --b=200 --te=0 --tr=0 --tm=0 " + flag,
                             submit_params={'logdir': tree_disp.get('log'),
                                            'wait_for': dti_job + merge_job})
            else:
                disp_job = None
            mask_disp(tree_disp, wait_for=disp_job)


if __name__ == '__main__':
    main()
