# Code and data accompanying "Improved fibre dispersion estimation using b-tensor encoding"
- Authors: Michiel Cottaar, Filip Szczepankiewicz, Matteo Bastiani, Moises Hernandez-Fernandez, Stamatios N. Sotiropoulos, Markus Nilsson, Saad Jbabdi
- The manuscript is available on [arxiv](https://arxiv.org/abs/1901.05820). It has been submitted to NeuroImage.
- Note that this code was designed just to process the data presented in this work.
    You are welcome to use it for other purposes, but there might be difficulties in adapting the code.
    Please report any problems with this code to MichielCottaar@pm.me .

## Installation
While the pipeline code is included here, the scripts to run the analysis are in my general repository of code & scripts (https://git.fmrib.ox.ac.uk/ndcn0236/mcutils).

The version of the code used to process the data presented in the paper is available as:
```shell script
pip install git+https://git.fmrib.ox.ac.uk/ndcn0236/mcutils.git@paper_MDE_v1
```

The newest version of this code (which might include additional bug fixes) can be installed using:
```shell script
pip install git+https://git.fmrib.ox.ac.uk/ndcn0236/mcutils.git
```

In addition FSL 6.0.1 or later is required.

## Monte Carlo data
Jupyter notebooks to regenerate the Monte Carlo experiments reported in the paper can be found in the `notebooks` folder.
These notebooks include:
- [ratio_micro_anisotropy.ipynb](notebooks/ratio_micro_anisotropy.ipynb): Generates Figure 2 in the paper.
- [monte_carlo.ipynb](notebooks/monte_carlo.ipynb): Produces all the plots based on the Monte Carlo simulations (Figures 3-5 and S2-S4).
    To speed up the evaluation 500 Monte Carlo simulations have already been generated (stored in csv files).
    Code to rerun these Monte Carlo simulations are included in the jupyter notebook.

## Running the pipeline
The pipeline can be run using
```shell script
python pipeline.py
```
This will run the pre-processing and analysis pipeline producing the output files defined in the [data.tree]() file.

Input data should be stored in data/{subject ID}/raw/ (at least for the default [data.tree]()). Here we expect to find:
- T1w.nii.gz (reference structural)
- b0_AP.nii.gz (full brain)
- b0_PA.nii.ga (full brain)
- LTE_{acquisition}.bvals
- LTE_{acquisition}.bvecs
- LTE_{acquisition}.nii.gz
- STE_{acquisition}.nii.gz
- STE_{acquisition}.bvals

Here `{acquisition}` is an identifier of the type of acquisition used. In our dataset this is one of "std", "short_TE",
"long_TE", "long_diff". The data presented in the paper is available in this format on reasonable request.

If run on a cluster, the code will use `fsl_sub` to try to submit the jobs. 
The queue names are hard-coded in the code and might need updating to run successfully.
If a cluster is not available, all the code will run in sequence, which might take several days.



